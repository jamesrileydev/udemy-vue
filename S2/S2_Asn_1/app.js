const app = Vue.createApp({
  data() {
    return {
      age: 47,
      name: 'Jim',
      favNum: 0,
      imgUrl: "https://unsplash.com/photos/XW7hPn3toZ8"
    }
  },
  computed: {
    agePlusFive: function () {
      return this.age + 5;
    }
  },
  methods: {
    returnFavNum(){
      return Math.random();
    }
  }
});

app.mount('#assignment');