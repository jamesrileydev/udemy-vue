const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      name: "",
      confirmedName: ''
    };
  },
  methods: {
    add(num){
      this.counter = this.counter + num;
    },
    confirmName(){
      this.confirmedName = this.name;
    },
    reduce(){
      this.counter > 0 ? this.counter-- : 0;
    },
    setName(e, lastName){
      console.log("setName called");
      this.name = e.target.value + ' ' + lastName;
    },
    submitForm(){
      //e.preventDefault();
      alert('Submit form called');
    }
  }
});

app.mount('#events');