const app = Vue.createApp({
  data() {
    return {
      message: 'My Course Goal',
      goalA: 'Learn Vue',
      goalB: 'Master Vue',
      html: '<p>Create some Vue apps</p>',
      vueLink: 'https://vuejs.org'
    };
  },
  methods: {
    outputGoal() {
      const rand = Math.random();
      return rand > 0.5 ? this.goalA : this.goalB;
    }
  }
})

app.mount('#user-goal');