const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      name: '', 
      confirmedName: ''
    };
  },
  computed: {
    fullName(){
      if(this.name === ''){
        return '';
      }
      return this.name + ' Riley'
    }
  },
  watch: {
    counter(value) {
      console.log('watch counter called. value: ' + value);
      if (value > 50) {
        this.counter = 0;
      }
    }
  },
  methods: {
    setName(event) {
      this.name = event.target.value;
    },
    add(num) {
      this.counter = this.counter + num;
    },
    reduce(num) {
      console.log(num);
      this.counter > 0 ? this.counter = this.counter - num : 0;
    },
    resetInput(){
      this.name = '';
    }
  }
});

app.mount('#events');
