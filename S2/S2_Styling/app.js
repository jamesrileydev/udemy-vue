const app = Vue.createApp({
  data() {
    return {
      boxAselected: false,
      boxBselected: false,
      boxCselected: false
    }
  },
  computed: {
  },
  methods: {
    boxClasses(box) {
      switch (box) {
        case 'A':
          return { active: this.boxAselected }
        case 'B':
          return { active: this.boxBselected }
        case 'C':
          return { active: this.boxCselected }
        default:
          break;
      }
    },
    boxSelected(box) {
      if (box === 'A') {
        this.boxAselected = !this.boxAselected;
      } else if (box === 'B') {
        this.boxBselected = !this.boxBselected;
      } else if (box === 'C') {
        this.boxCselected = !this.boxCselected;
      }
    }
  }
});
app.mount('#styling');