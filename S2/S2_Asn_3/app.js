const app = Vue.createApp({
  data() {
    return {
      total: 0
    }
  },
  methods: {
    add(num) {
      this.total = this.total + num;
      console.log('total: ' + this.total);
      console.log('Result: ' + this.result);
    }
  },
  computed: {
    result() {
      if (this.total === 37) {
        return 'You reached 37';
      } else if (this.total < 37) {
        return 'Not there yet';
      } else {
        return 'Too much!';
      }
    }
  },
  watch: {
    total(value) {
      if (value > 37) {
        setTimeout(() => {
          this.total = 0;
        }, 5000)
      }
    }
  }
});

app.mount('#assignment');