const app = Vue.createApp({
  data() {
    return {
      inputStyle: '',
      isVisible: true,
      inputColor: ''
    };
  },
  methods: {
    toggleVisibility() {
      this.isVisible = !this.isVisible;
    }
  },
  computed: {
    visibleClasses() {
      return {
        visible: this.isVisible,
        hidden: !this.isVisible
      }
    }
  }
});

app.mount('#assignment');