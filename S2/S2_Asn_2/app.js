const app = Vue.createApp({
  data(){
    return{
      value: '',
      confirmedValue: ''
    }
  },
  methods: {
    showAlert(){
      alert("showAlert called");
    },
    showValue(e){
      this.value = e.target.value;
    },
    confirmValue(e){
      this.confirmedValue = e.target.value;
    }
  }
});

app.mount('#assignment');