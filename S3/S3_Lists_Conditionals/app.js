const app = Vue.createApp({
  data() {
    return { 
      enteredGoal: '',
      goals: [],
      testObject: {"name" : "Jim", "age": 47}
    };
  },
  methods: {
    addGoal(){
      this.goals.push(this.enteredGoal);
      this.enteredGoal = '';
      console.log(this.goals);
    },
    removeGoal(idx){
      console.log('removeGoal called')
      this.goals.splice(idx, 1);
    }
  }
});

app.mount('#user-goals');
