const app = Vue.createApp({
    data() {
        return {
            task: '',
            taskList: [],
            shouldShow: true
        }
    },
    computed: {
        buttonCaption(){
           return this.shouldShow ? 'Hide' : 'Show';
        }
    },
    methods: {
        addTask(){
            this.taskList.push(this.task);
            this.task = '';
            console.log(this.taskList);
        },
        removeTask(index){
            this.taskList.splice(index, 1);
        },
        showList(){
            console.log('showList called');
            this.shouldShow = !this.shouldShow;
        }
    }
});

app.mount('#assignment');